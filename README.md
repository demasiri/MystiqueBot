# Mystique Bot
Mystique is a Discord Bot based heavily on the base code of Red (Cogs-Creators) by Twentysix26. Much of the base functionality is from that project, and I take no credit for it.

What Mystique does - like her X-Men comic book universe namesake - is assumes the role of many different personalities that have been 'programmed' and can be easily extended. This gives the bot its own flavour and 'personality'. You can use one of the base personalities or, if you feel like it, create your very own.

More docs to follow as project progresses.

## Requirements
WIP...
* Discord
* ...