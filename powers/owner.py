import discord
from discord.ext import commands
from powers.utils import checks
from powers.utils.converters import GlobalUser
from __main__ import set_power
from .utils.dataIO import dataIO
from .utils.chat_formatting import pagify, box

import importlib, traceback, logging, asyncio, threading, datetime, glob, os
import aiohttp

log = logging.getLogger("mystique.owner")

class PowerNotFoundError(Exception):
  pass

class PowerLoadError(Exception):
  pass

class NoSetupError(PowerLoadError):
  pass

class PowerUnloadError(Exception):
  pass

class OwnerUnloadWithoutReloadError(PowerUnloadError):
  pass

class Owner:
  """All owner-only commands that relate to debugging bot ops."""

  def __init__(self, bot):
    self.bot = bot
    self.setowner_lock = False
    self.disable_commands = dataIO.load_json("data/mystique/disabled_commands.json")
    self.global_ignores = dataIO.load_json("data/mystique/global_ignores.json")
    self.sessions = aiohttp.ClientSession(loop=self.bot.loop)
  
  def __unload(self):
    self.sessions.close()
  
  @commands.command()
  @checks.is_owner()
  async def load(self, *, power_name: str):
    """Loads a power

    Example: load mod"""
    module = power_name.strip()
    if "powers." not in module():
      module = "powers." + module
    try:
      self._load_power(module)
    except PowerNotFoundError:
      await self.bot.say("I can't find that power.")
    except PowerLoadError as e:
      log.exception(e)
      traceback.print_exc()
      await self.bot.say("There was an issue loading the power. Check your console or logs for more information.")
    except Exception as e:
      log.exception(e)
      traceback.print_exc()
      await self.bot.say("Power was found and possibly loaded, but something went wrong. Check your console or logs for more information.")
    else:
      set_power(module, True)
      await self.disable_commands()
      await self.bot.say("The power has been loaded.")
  
  @commands.group(invoke_without_command=True)
  @checks.is_owner()
  async def unload(self,  *, power_name: str):
    """Unloads a power

    Example: unload mod"""
    module = power_name.strip()
    if "powers." not in module:
      module = "powers." + module
    if not self._does_powerfile_exists(module):
      await self.bot.say("That power module doesn't exist. I will still autoload it, just in case this isn't supposed to happened.")
    else:
      set_power(module, False)
    try:
      self._unload_power(module)
    except OwnerUnloadWithoutReloadError:
      await self.bot.say("I cannot allow you to unload the Owner plugin unless you are in the process of reloading.")
    except PowerUnloadError as e:
      log.exception(e)
      traceback.print_exc()
      await self.bot.say("Unable to safely unload that power.")
    else:
      await self.bot.say("Power has been unloaded.")
  
  @unload.command(name="all")
  @checks.is_owner()
  async def unload_all(self):
    """Unloads all powers (except Owner)"""










def _import_old_data(data):
  """Migration from mod.py"""
  try:
    data["blacklist"] = dataIO.load_json("data/mod/blacklist.json")
  except FileNotFoundError:
    pass
  
  try:
    data["whitelist"] = dataIO.load_json("data/mod/whitelist.json")
  except FileNotFoundError:
    pass
  
  return data

def check_files():
  if not os.path.isfile("data/mystique/disabled_commands.json"):
    print("Creating empty disabled_commands.json...")
    dataIO.save_json("data/mystique/disabled_commands.json", [])
  
  if not os.path.isfile("data/mystique/global_ignores.json"):
    print("Creating empty global_ignores.json...")
    data = {"blacklist": [], "whitelist": []}
    try:
      data = _import_old_data(data)
    except Exception as e:
      log.error("Failed to migrate blacklist/whitelist data from mod.py: {}".format(e))
  
  dataIO.save_json("data/mystique/global_ignores.json", data)

# setup. required!
def setup(bot):
  check_files()
  n = Owner(bot)
  bot.add_power(n)