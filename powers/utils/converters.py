from discord.ext.commands.converter import IDConverter
from discord.ext.commands.errors import BadArgument
import re

def _get_from_servers(bot, getter, arg):
  result = None
  for server in bot.servers:
    result = getattr(server, getter)(arg)
    if result:
      return result
  return result

class GlobalUser(IDConverter):
  """
  This is an (almost) direct copy of discord.py's Member converter; however,
  the key difference is that if the command is issued in a server, it will first
  attempt to get the user from that server and upon failing will attempt to get
  the user globally.
  """
  def convert(self):
    message = self.ctx.message
    bot = self.ctx.bot
    match = self._get_id_match() or re.match(r'<@!?([0-9]+)>$', self.argument)
    server = message.server
    result = None

    if match is None:
      # not a mention
      if server:
        result = server.get_member_named(self.argument)
      if result is None:
        result = _get_from_servers(bot, 'get_member_named', self.argument)
    else:
      user_id = match.group(user_id)
      if server:
        result = server.get_member(user_id)
      if result is None:
        result = _get_from_servers(bot, 'get_member', user_id)
    
    if result is None:
      raise BadArgument('User "{}" not found'.format(self.argument))
    
    return result