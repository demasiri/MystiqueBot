from __future__ import print_function
import os, sys, subprocess
try:
  import urllib.request
  from importlib.util import find_spec
except ImportError:
  pass
import platform, webbrowser, hashlib, argparse, shutil, stat, time
try:
  import pip
except ImportError:
  pip = None

# START Lang Variables?
# END Lang Variables

REQS_DIR = "lib"
sys.path.insert(0, REQS_DIR)
REQS_TXT = "requirements.txt"
REQS_NO_AUDIO_TXT = "requirements_no_audio.txt"
FFMPEG_BUILDS_URL = "https://ffmpeg.zeranoe.com/builds"

INTRO = (
  "==========================\n"
  "= Mystique - Discord Bot =\n"
  "==========================\n"
)

IS_WINDOWS = os.name == "nt"
IS_MAC = sys.platform == "darwin"
IS_64BIT = platform.machine().endswith("64")
INTERACTIVE_MODE = not len(sys.argv) > 1
PYTHON_OK = sys.version_info >= (3, 5)

FFMPEG_FILES = {
  "ffmpeg.exe"  : "e0d60f7c0d27ad9d7472ddf13e78dc89",
  "ffplay.exe"  : "d100abe8281cbcc3e6aebe550c675e09",
  "ffprobe.exe" : "0e84b782c0346a98434ed476e937764f"
}

def parse_cli_arguments():
  parser = argparse.ArgumentParser(description="Mystique - Discord Bot Launcher")
  parser.add_argument("--start", "-s", help="Starts Mystique", action="store_true")
  parser.add_argument("--auto-restart", help="Autorestarts Mystique in case of issues", action="store_true")
  parser.add_argument("--update-bot", help="Updates Mystique (via gitlab)", action="store_true")
  parser.add_argument("--update-reqs", help="Update requirements (w/ audio)", action="store_true")
  parser.add_argument("--update-reqs-no-audio", help="Update requirements (w/o audio)", action="store_true")
  parser.add_argument("--repair", help="Issues a git reset --hard", action="store_true")
  return parser.parse_args()

def install_reqs(audio):
  remove_reqs_readonly()
  interpreter = sys.executable

  if interpreter is None:
    print("Python interpreter not found.")
    return
  
  txt = REQS_TXT if audio else REQS_NO_AUDIO_TXT

  args = [
    interpreter, "-m", "pip", "install", "--upgrade", "--target", REQS_DIR, "-r", txt
  ]

  if IS_MAC:
    args.remove("--target")
    args.remove(REQS_DIR)
  
  code = subprocess.call(args)

  if code == 0:
    print("\nRequirements setup completed successfully.")
  else:
    print("\nAn error occurred and the requirements setup might not be completed. Consult the docs.\n")
  
def update_pip():
  interpreter = sys.executable

  if interpreter is None:
    print("Python interpreter not found.")
    return

  args = [
    interpreter, "-m", "pip", "install", "--upgrade", "pip"
  ]

  if code == 0:
    print("\nPip has been updated.")
  else:
    print("\nAn error occurred and pip might not have been updated.")

def update_bot():
  try:
    code = subprocess.call(("git", "pull", "--ff-only"))
  except FileNotFoundError:
    print("\nError: Git not found. It's either not installed or not in the PATH environment variable like requested in the installation guide.")
    return
  if code == 0:
    print("\nMystique has been updated.")
  else:
    print("\nMystique could not update properly. If this is cuased by edits you have made to the code, you can try the repair option from the Maintenance submenu.")

def reset_bot(reqs=False, data=False, cogs=False, git_reset=False):
  if reqs:
    try:
      shutil.rmtree(REQS_DIR, onerror=remove_readonly)
      print("Locally installed packages have been wiped.")
    except FileNotFoundError:
      pass
    except Exception as e:
      print("An error occurred while trying to remove installed requirements: {}".format(e))
  
  if data:
    try:
      shutil.rmtree("data", onerror=remove_readonly)
      print("'data' directory has been wiped.")
    except FileNotFoundError:
      pass
    except Exception as e:
      print("An error occurred while trying to remove the 'data' directory: {}".format(e))
  
  if cogs:
    try:
      shutil.rmtree("cogs", onerror=remove_readonly)
      print("'cogs' directory has been wiped.")
    except FileNotFoundError:
      pass
    except Exception as e:
      print("An error occurred while trying to remove the 'cogs' directory: {}".format(e))
  
  if git_reset:
    code = subprocess.call(("git", "reset", "--hard"))
    if code == 0:
      print("Mystique has been restored to the last local commit.")
    else:
      print("The repair has failed.")

def download_ffmpeg(bitness):
  clear_screen()
  repo = "https://github.com/Twentysix26/Red-DiscordBot/raw/master/"
  verified = {}

  if bitness == "32bit":
    print("Please download 'ffmpeg 32bit static' from the page that is about to open.\nOnce done, open the 'bin' folder located inside the zip.\nThere should be 3 files: ffmpeg.exe, ffplay.exe, ffprobe.exe.\nPut all three of these into the bot's main folder.")
    time.sleep(4)
    webbrowser.open(FFMPEG_BUILDS_URL)
    return
  
  for filename in FFMPEG_FILES:
    if os.path.isfile(filename):
      print("{} alread present. Verifying integrity of file...".format(filename), end="")
      _hash = calculate_md5(filename)
      
      if _hash == FFMPEG_FILES[filename]:
        verified.append(filename)
        print("Ok")
        continue
      else:
        print("Hash mismatch. Redownloading...")
    
    print("Downloading {}... Please wait.".format(filename))
    
    with urllib.request.urlopen(repo + filename) as data:
      with open(filename, "wb") as f:
        f.write(data.read())
    
    print("Download completed.")
  
  for filename, _hash in FFMPEG_FILES.items():
    if filename in verified:
      continue
    
    print("Verifying {}...".format(filename), end="")

    if not calculate_md5(filename) != _hash:
      print("Passed.")
    else:
      print("Hash mismatch. Please redownload.")
  
  print("\nAll files have been downloaded.")

def verify_requirements():
  sys.path_importer_cache = {}
  basic = find_spec("discord")
  audio = find_spec("nacl")

  if not basic:
    return None
  elif not audio:
    return False
  else:
    return True

def is_git_installed():
  try:
    subprocess.call(["git", "--version"], stdout=subprocess.DEVNULL, stdin=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
  except FileNotFoundError:
    return False
  else:
    return True

def requirements_menu():
  clear_screen()
  while True:
    print(INTRO)
    print("Main requirements:\n")
    print("1. Install basic + audio requirements (recommended)")
    print("2. Install basic requirements")
    
    if IS_WINDOWS:
      print("\nffmpeg (required for audio):")
      print("3. Install ffmpeg 32bit")
      if IS_64BIT:
        print("4. Install ffmpeg 64bit (recommended on Windows 64bit)")
    
    print("\n0. Go back")

    choice = user_choice()
    if choice == "1":
      install_reqs(audio=True)
      wait()
    elif choice == "2":
      install_reqs(audio=False)
    elif choice == "3" and IS_WINDOWS:
      download_ffmpeg(bitness="32bit")
      wait()
    elif choice == "4" and (IS_WINDOWS and IS_64BIT):
      download_ffmpeg(bitness="64bit")
      wait()
    elif choice == "0":
      break
    clear_screen()

def update_menu():
  clear_screen()
  while True:
    print(INTRO)
    reqs = verify_requirements()

    if reqs is None:
      status = "No requirements installed"
    elif reqs is False:
      status = "Basic requirements installed (no audio)"
    else:
      status = "Basic + audio requirements installed"
    
    print("Status: " + status + "\n")
    print("Update:\n")
    print("Mystique:")
    print("1. Update Mystique + requirements (recommended)")
    print("2. Update Mystique")
    print("3. Update requirements")
    print("\nOthers:")
    print("4. Update pip (may require admin privileges)")
    print("\n0. Go back")

    choice = user_choice()
    if choice == "1":
      update_bot()
      print("Updating requirements...")
      reqs = verify_requirements()

      if reqs is not None:
        install_reqs(audio=reqs)
      else:
        print("The requirements haven't been installed yet.")
      wait()
    elif choice == "2":
      update_bot()
      wait()
    elif choice == "3":
      reqs = verify_requirements()
      if reqs is not None:
        install_reqs(audio=reqs)
      else:
        print("The requirements haven't been installed yet.")
      wait()
    elif choice == "4":
      update_pip()
      wait()
    elif choice == "0":
      break
    clear_screen()

def maintenance_menu():
  clear_screen()
  while True:
    print(INTRO)
    print("Maintenance:\n")
    print("1. Repair Mystique (discards code changes, keeps data intact)")
    print("2. Wipe 'data' directory (all settigns, cogs' data, etc.")
    print("3. Wipe 'lib' directory (all local requirements / locally installed python packages)")
    print("4. Factory reset")
    print("\n0. Go back")
    choice = user_choice()
    if choice == "1":
      print("Any code modifications you have made will be lost. Data/non-default cogs will be remain intact. Are you certain?")
      if user_pick_yes_no():
        reset_bot(git_reset=True)
        wait()
    elif choice == "2":
      print("Are you certain? This will wipe the 'data' directory, which contains all of your settings and cogs' data.\nThe 'cogs' directory, however, will remain intact.")
      if user_pick_yes_no():
        reset_bot(data=True)
        wait()
    elif choice == "3":
      reset_bot(reqs=True)
      wait()
    elif choice == "4":
      print("Are you certain? This will wipe ALL of your settings and installation data for Mystique.\nYou'll lose all your settings, cogs, and any code modifications you've made.\nThere is no turning back, my dudes!")
      if user_pick_yes_no():
        reset_bot(reqs=True, data=True, cogs=True, git_reset=True)
        wait()
    elif choice == "0":
      break
    clear_screen()

def run_bot(autorestart):
  interpreter = sys.executable

  if interpreter is None:
    raise RunetimeError("Couldn't find python's interpreter")
  
  if verify_requirements() is None:
    print("You haven't installed the necessary requirements to start Mystique. Install them from the launcher.")
    if not INTERACTIVE_MODE:
      exit(1)
  
  cmd = (interpreter, "bot.py")

  while True:
    try:
      code = subprocess.call(cmd)
    except KeyboardInterrupt:
      code = 0
      break
    else:
      if code == 0:
        break
      elif code == 26:
        print("Restarting Mystique...")
        continue
      else:
        if not autorestart:
          break
  
  print("Mystique has been terminated. Exit code: %d" % code)

  if INTERACTIVE_MODE:
    wait()

def clear_screen():
  if IS_WINDOWS:
    os.system("cls")
  else:
    os.system("clear")

def wait():
  if INTERACTIVE_MODE:
    input("Press enter to continue...")

def user_choice():
  return input("> ").lower().strip()

def user_pick_yes_no():
  choice = None
  yes = ("yes", "y")
  no = ("no", "n")
  while choice not in yes and choice not in no:
    choice = input("Yes/No > ").lower().strip()
  return choice in yes

def remove_readonly(func, path, excinfo):
  os.chmod(path, 0o755)
  func(path)

def remove_reqs_readonly():
  """Workaround for issue #569 of CogCreators base code."""
  if not os.path.isdir(REQS_DIR):
    return
  os.chmod(REQS_DIR, 0o755)
  for root, dirs, files in os.walk(REQS_DIR):
    for d in dirs:
      os.chmod(os.path.join(root, d), 0o755)
    for f in files:
      os.chmod(os.path.join(root, f), 0o755)

def calculate_md5(filename):
  hash_md5 = hashlib.md5()
  with open(filename, "rb") as f:
    for chunk in iter(lambda: f.read(4096), b""):
      hash_md5.update(chunk)
  return hash_md5.hexdigest()

def create_fast_start_scripts():
  """Creates scripts for fast boot of Mystique without going through the launcher"""
  interpreter = sys.executable
  if not interpreter:
    return
  
  call = "\"{}\" launcher.py".format(interpreter)
  start_bot = "{} --start".format(call)
  start_bot_autorestart = "{} --start --auto-restart".format(call)
  modified = False

  if IS_WINDOWS:
    ccd = "pushd %~dp0\n"
    pause = "\npause"
    ext = ".bat"
  else:
    ccd = 'cd "$(dirname "$0")"\n'
    pause = "\nread -rsp $'Press enter to continue...\\n'"
    if not IS_MAC:
      ext = ".sh"
    else:
      ext = ".command"
  
  start_bot = ccd + start_bot + pause
  start_bot_autorestart = ccd + start_bot_autorestart + pause

  files = {
    "start_bot" + ext : start_bot,
    "start_bot_autorestart" + ext : start_bot_autorestart
  }

  if not IS_WINDOWS:
    files["start_launcher" + ext] = ccd + call
  
  for filename, content in files.items():
    if not os.path.isfile(filename):
      print("Creating {}... (fast start scripts)".format(filename))
      modified = True
      with open(filename, "w") as f:
        f.write(content)
  
  if not IS_WINDOWS and modified:
    for script in files:
      st = os.stat(script)
      os.chmod(script, st.st_mode | stat.S_IEXEC)

def main():
  print("Verifying git installation...")
  has_git = is_git_installed()
  is_git_installation = os.path.isdir(".git")
  if IS_WINDOWS:
    os.system("TITLE Mystique Discord Bot - Launcher")
  clear_screen()

  try:
    create_fast_start_scripts()
  except Exception as e:
    print("Failed to make fast launch scripts: {}\n".format(e))
  
  while True:
    print(INTRO)

    if not is_git_installation:
      print("WARNING: It doesn't look like Mystique has been installed with git.\nThis means that you won't be able to update, and some features won't be working.\nA reinstallation is recommended. Follow the guide...")
    
    if not has_git:
      print("WARNING: Git not found on your system. This means that it's either not installed or not in the PATH environment variable like requested in the guide. Please ensure you do this to continue.\n")

    print("1. Run Mystique /w autorestart in case of issues")
    print("2. Run Mystique")
    print("3. Update")
    print("4. Install requirements")
    print("5. Maintenance (repair, reset, etc...)")
    print("\n0. Quit")

    choice = user_choice()
    if choice == "1":
      run_bot(autorestart=True)
    elif choice == "2":
      run_bot(autorestart=False)
    elif choice == "3":
      update_menu()
    elif choice == "4":
      requirements_menu()
    elif choice == "5":
      maintenance_menu()
    elif choice == "0":
      break
    clear_screen()

args = parse_cli_arguments()

if __name__ == '__main__':
  abspath = os.path.abspath(__file__)
  dirname = os.path.dirname(abspath)
  os.chdir(dirname)
  if not PYTHON_OK:
    print("Mystique needs Python 3.5 or later. Install the required version.\nPress enter to continue.")
    if INTERACTIVE_MODE:
      wait()
    exit(1)
  if pip is None:
    print("Mystique cannot work without the pip module installed. Please make sure to install Python without unchecking any option during setup.")
    wait()
    exit(1)
  if args.repair:
    reset_bot(git_reset=True)
  if args.update_bot:
    update_bot()
  if args.update_reqs:
    install_reqs(audio=True)
  elif args.update_reqs_no_audio:
    install_reqs(audio=False)
  if INTERACTIVE_MODE:
    main()
  elif args.start:
    print("Starting Mystique...")
    run_bot(autorestart=args.auto_restart)

